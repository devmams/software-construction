## Interval class implementation

Use the "Test-Driven Development" approach to  implement the UML class `Interval`, its attributes and operations. 


![](CD-Interval.png)


Part of the implementation is already done: a Java class named `Interval` containing 
two methods is available, as well as several test cases.
There is only a partial implementation of the test cases: only the method comments and its signature are available.
You have to write the body of the test methods.

Follow these steps to achieve the implementation of the method `Interval::includes()`:

  1. Start writing the test cases for the method. Read the comments attentively.
  2. Then, implement the method and check that all test pass. 
  3. Improve the quality of the code source: passing all tests is not enough, the code must be readable.
  4. Improve the tests, if you estimate that some cases were not tested.
  5. You are done for the method. 
  
Now start again the above steps to implement the method `Interval::overlapsWith()`.
