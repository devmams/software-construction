package fr.unantes.software.construction.agenda;

/**
 * Created on 13/02/2018.
 *
 *
 */
public class Interval<T extends Comparable> {

    private final T begin;
    private final T end;

    protected Interval(T begin, T end) {
        if(!(begin.compareTo(end) == -1)){
            throw new IllegalArgumentException();
        }
        this.begin = begin;
        this.end = end;
    }

    public boolean includes(T value) {
        if(((begin.compareTo(value) == -1) && end.compareTo(value) == 1)
                || begin.compareTo(value) == 0 || end.compareTo(value) == 0){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean overlapsWith(Interval<T> other) {
        if(other.includes(begin)){
            return  true;
        }
        else if(other.includes(end)){
            return true;
        }
        else if(other.includes(begin) && other.includes(end)){
            return true;
        }
        else{
            return false;
        }
    }

    public T getBegin(){
        return begin;
    }

    public T getEnd(){
        return end;
    }
}

