package fr.unantes.software.construction.agenda;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.omg.PortableInterceptor.INACTIVE;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * Created on 21/02/2018.
 *
 */
class IntervalTest {

    /**
     * Checks if the Interval::includes() methods returns true for values inside the interval.
     * Uses the inner boundaries as test data, as well as some values between them.
     */
    @Test
    void testIncludes() {
        Interval<Integer> intervalInt = new Interval<>(1,10);
        assertTrue(intervalInt.includes(1));
        assertTrue(intervalInt.includes(5));
        assertTrue(intervalInt.includes(10));
    }

    /**
     *  Checks if the Interval::includes() methods returns true for values outside the interval.
     *  Uses the outer boundaries as test data, as well as Interger.MIN_VALUE and
     *  Integer.MAX_VALUE
     *
     */
    @Test
    void testNotIncludes() {
        Interval<Integer> intervalInt = new Interval<Integer>(1,10);
        assertFalse(intervalInt.includes(0));
        assertFalse(intervalInt.includes(11));
        assertFalse(intervalInt.includes(Integer.MIN_VALUE));
        assertFalse(intervalInt.includes(Integer.MAX_VALUE));
    }

    /**
     * Given the intervals [a,b] and [c,d], checks if the Interval::overlapsWith()
     * method returns true for the case where:
     * [a,b] contains c.
     */
    @Test
    void testOverlapsWithOtherFirst() {
        Interval<Integer> intervalAToB = new Interval<Integer>(1,10);
        Interval<Integer> intervalCToD = new Interval<Integer>(5,15);

        assertTrue(intervalAToB.includes(intervalCToD.getBegin()));
    }

    /**
     * Given the intervals [a,b] and [c,d], checks if the Interval::overlapsWith()
     * method returns true for the case where:
     * [a,b] contains d.
     */
    @Test
    void testOverlapsWithOtherLast() {
        Interval<Integer> intervalAToB = new Interval<Integer>(1,10);
        Interval<Integer> intervalCToD = new Interval<Integer>(2,8);

        assertTrue(intervalAToB.includes(intervalCToD.getEnd()));
    }

    /**
     * Given the intervals [a,b] and [c,d], checks if the Interval::overlapsWith()
     * method returns true for the case where:
     * [a,b] contains c and d.
     */
    @Test
    void testOverlapsWithOtherBoth() {
        Interval<Integer> intervalAToB = new Interval<Integer>(1,10);
        Interval<Integer> intervalCToD = new Interval<Integer>(2,8);

        assertTrue(intervalAToB.includes(intervalCToD.getBegin())
                && intervalAToB.includes(intervalCToD.getEnd()));
    }

    /**
     * Given the intervals [a,b] and [c,d], checks if the Interval::overlapsWith()
     * is commutative: if [a,b] overlaps with [c,d], then [c,d] also overlaps with
     * [a,b].
     */
    @Test
    void testOverlapsWithIsCommutative() {
        Interval<Integer> intervalAToB = new Interval<Integer>(1,10);
        Interval<Integer> intervalCToD = new Interval<Integer>(5,15);

        assertTrue(intervalAToB.overlapsWith(intervalCToD));
        assertTrue(intervalCToD.overlapsWith(intervalAToB));

    }

    /**
     * Given the intervals [a,b] and [c,d], checks if the Interval::overlapsWith()
     * method returns false for the case where:
     * c < a and d < a.
     */
    @Test
    void testNotOverlapsWithInferiorValues() {
        Interval<Integer> intervalAToB = new Interval<Integer>(1,10);
        Interval<Integer> intervalCToD = new Interval<Integer>(-5,0);

        assertFalse(intervalAToB.overlapsWith(intervalCToD));

    }

    /**
     * Given the intervals [a,b] and [c,d], checks if the Interval::overlapsWith()
     * method returns false for the case where:
     * c > b and d > b.
     */
    @Test
    void testNotOverlapsWithSuperiorValues() {
        Interval<Integer> intervalAToB = new Interval<Integer>(1,10);
        Interval<Integer> intervalCToD = new Interval<Integer>(11,15);

        assertFalse(intervalAToB.overlapsWith(intervalCToD));
    }
}