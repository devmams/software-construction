package fr.unantes.software.construction.associations;

public class SingleReferenceEventToTask implements SingleReference<Event>{

    private Event event;
    private Task task;

    public SingleReferenceEventToTask(Task task){
        event = null;
        this.task = task;
    }


    @Override
    public void set(Event newValue) {

        if(newValue.task().isSet()){
            newValue.task().get().event().basicUnSet();
        }
        if(isSet()){
            event.task().unset();
            basicUnSet();
        }
        newValue.task().basicSet(task);
        this.basicSet(newValue);

    }

    @Override
    public Event get() {
        return event;
    }

    @Override
    public void unset() {
        if(isSet()) {
            event.task().basicUnSet();
        }
        this.basicUnSet();
    }

    @Override
    public boolean isSet() {
        return !(event == null);
    }

    @Override
    public void basicSet(Event newValue) {
        event = newValue;
    }

    @Override
    public void basicUnSet() {
        event = null;
    }
}
