package fr.unantes.software.construction.associations;

public class SingleReferenceCalToEvent implements SingleReference<Calendar>{

    private Event event;
    private Calendar container;


    public SingleReferenceCalToEvent(Event event){
        this.event = event;
    }

    @Override
    public void set(Calendar newValue) {
        if(event.calendar().isSet()){
            event.calendar().unset();
        }
        this.basicSet(newValue);
        newValue.events().basicAdd(event);
    }

    @Override
    public Calendar get() {
        return container;
    }

    @Override
    public void unset() {
        container.events().basicRemove(event);
        this.basicUnSet();
    }

    @Override
    public boolean isSet() {
        return !(container == null);
    }

    @Override
    public void basicSet(Calendar newValue) {
        container = newValue;
    }

    @Override
    public void basicUnSet() {
        container = null;
    }
}
