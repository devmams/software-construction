package fr.unantes.software.construction.associations;

import java.util.ArrayList;
import java.util.List;

public class MultipleReferenceEventToCal implements MultipleReference<Event> {
    private List<Event> events = new ArrayList<>();
    private Calendar calendar;

    public MultipleReferenceEventToCal(Calendar calendar){
        this.calendar = calendar;
    }

    @Override
    public boolean add(Event value) {
        if(value.calendar().isSet()){
            value.calendar().unset();
        }
        value.calendar().basicSet(calendar);
        return events.add(value);
    }

    @Override
    public void remove(Event value) {
        value.calendar().basicUnSet();
        this.basicRemove(value);
    }

    @Override
    public boolean contains(Event value) {
        return events.contains(value);
    }

    @Override
    public int size() {
        return events.size();
    }

    @Override
    public void basicAdd(Event value) {
        events.add(value);
    }

    @Override
    public void basicRemove(Event value) {
        events.remove(value);
    }
}
