package fr.unantes.software.construction.associations;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 27/01/2018.
 *
 * @author sunye.
 */
public class SimpleEvent implements Event {

    private Integer id;
    private String name;
    private String location;
    private List<Integer> alarm = new ArrayList<>(5);
    private SingleReference<Calendar> cal = new  SingleReferenceCalToEvent(this);
    private SingleReference<Task> task = new SingleReferenceTaskToEvent(this);
    private MultipleReference<Contact> contact = new MutipleContactReference();

    public SimpleEvent(Integer id) {
        this.id = id;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String str) {
        name = str;
    }

    @Override
    public String getLocation() {
        return location;
    }

    @Override
    public void setLocation(String str) {
        location = str;
    }

    @Override
    public boolean addAlarm(Integer i) {
        if(alarm.size()<5){
            return alarm.add(i);
        }
        return false;
    }

    @Override
    public Integer getAlarm(int i) {
        return alarm.get(i);
    }

    @Override
    public Integer removeAlarm(int i) {
        return alarm.remove(i);
    }

    @Override
    public SingleReference<Calendar> calendar() {
        return cal;
    }

    @Override
    public SingleReference<Task> task() {
        return task;
    }

    @Override
    public MultipleReference<Contact> invitees() {
        return contact;
    }

}
