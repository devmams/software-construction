package fr.unantes.software.construction.associations;

public class SingleReferenceTaskToEvent implements SingleReference<Task> {

    private Task task;
    private Event event;


    public SingleReferenceTaskToEvent(Event event){
        task = null;
        this.event = event;
    }

    @Override
    public void set(Task newValue) {

        if(newValue.event().isSet()){
            newValue.event().get().task().basicUnSet();
        }
        if(isSet()){
            task.event().unset();
            basicUnSet();
        }
        newValue.event().basicSet(event);
        this.basicSet(newValue);



    }

    @Override
    public Task get() {
        return task;
    }

    @Override
    public void unset() {
//        task.event().basicUnSet();
        this.basicUnSet();
    }

    @Override
    public boolean isSet() {
        return !(task == null);
    }

    @Override
    public void basicSet(Task newValue) {
        task = newValue;
    }

    @Override
    public void basicUnSet() {
        task = null;
    }
}
