package fr.unantes.software.construction.associations;

/**
 * Created on 27/01/2018.
 *
 * @author sunye.
 */
public class SimpleCalendar implements Calendar {

    private String name;
    private String description;
    MultipleReference<Event> events = new MultipleReferenceEventToCal(this);


    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String str) {
        name = str;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String str) {
        description = str;
    }

    @Override
    public MultipleReference<Event> events() {
        return events;
    }

}
