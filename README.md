# Software Construction

## Setup

### Cloning the GitLab project

First of all, you clone copy the project containing all labs from the University GitLab.
As you will see, all the labs are stored in different directories.

To clone the project, use the following git command:

```shell
git clone https://gitlab.univ-nantes.fr/sunye-g/software-construction.git
```

### Tools

During the labs, you will use 3 different tools: Apache Maven, JUnit and IntelliJ IDEA.

- To understand Apache Maven, read the text «[Premiers pas avec Maven](https://sunye.github.io/java/maven/2018/01/14/maven.html)».
- To understand JUnit, read the text «[Les tests unitaires en Java](https://openclassrooms.com/courses/les-tests-unitaires-en-java)».
- IntelliJ IDEA is available at the JetBrain's [website](https://www.jetbrains.com/idea/).


## Labs
  1. [Attribute implementation](attributes/).
  2. [Association implementation](associations/).
  3. [Unit Tests](interval/).